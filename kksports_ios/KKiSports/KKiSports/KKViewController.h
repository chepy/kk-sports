//
//  KKViewController.h
//  KKClassmate
//
//  Created by Kelly Mr on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
//  KK's base controller for all to customize apperance.
//

#import <UIKit/UIKit.h>

@interface KKViewController : UIViewController

@end
