//
//  KKPicker.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KKPickerDelegate <NSObject>
@optional
-(void)pickerButtonOK:(id)sender;
-(void)pickerButtonCancel:(id)sender;
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
@end


@interface KKPicker : NSObject <UIPickerViewDelegate,UIPickerViewDataSource, KKPickerDelegate> 
{
    id _delegate;
    
    UIActionSheet* actionSheet;
	bool isPickerShow;
	UIPickerView* picker;
    
    UIView *_parentView;
    
    NSArray *_dataSource;
    
    // 默认选中
    NSInteger *_selectedComponent;  
    NSInteger *_selectedRow;
}

-(id)initWithParentView:(UIView *)parentView dataSource:(NSArray *)dataSource;
-(id)initWithParentView:(UIView *)parentView dataSource:(NSArray *)dataSource delegate:(id)theDelegate;

-(void)selectRow:(NSInteger)row inComponent:(NSInteger)component;
-(NSInteger)selectedRowInComponent:(NSInteger)component;

-(void)show;
-(void)hide;

@end

