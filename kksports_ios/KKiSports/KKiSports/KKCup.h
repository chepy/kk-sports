//
//  KKBox.h
//  kkjump
//
//  Created by Mrkelly on 12-3-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
//  公共容器，杯具, 实质一个公共字典，可以进行操控
//
//
#import <Foundation/Foundation.h>

//@interface KKCup : NSObject
//
//+(id)objectForKey:(id)aKey;
//+ (void)setObject:(id)anObject forKey:(id)aKey;
//
//@end

@interface KKCup : NSObject {
    
    NSMutableDictionary *_bigDict; //杯具大字典容器...
    
}

+ (KKCup *)shared;

// 获取设置
+(id)objectForKey:(id)aKey;

// 设置
+ (void)setObject:(id)anObject forKey:(id)aKey;




// Setter
-(void)setBigDict:(NSDictionary *)bigDict;

// Getter
-(NSMutableDictionary *)getBigDict;



@end