//
//  KKModel.m
//  KKiSports
//
//  Created by Kelly Mr on 12-5-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKModel.h"
#import "KKSettings.h"
#import "JSONKit.h"
#import "OpenUDID.h"

#import "KKMd5.h"

@implementation KKModel

-(id)init
{
    if ( self = [super init] )
    {
        m_apiSettings = [[NSMutableDictionary alloc] initWithContentsOfFile:
                         [[NSBundle mainBundle] pathForResource:@"APISettings" ofType:@"plist"]];
        
        
        // Base API URL地址
        NSString *apiUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"api_base_url"];  // from user defaults
        if ( apiUrl == nil )
        {
            apiUrl = [m_apiSettings objectForKey:@"base_url"];  // from file
        }
        _baseURL = apiUrl;
        
        _delegate = self; // 默认
        m_receivedData = [[NSMutableData alloc] init];
        
        // 预定义header...每一次请求都要传
        NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:@"savedPassword"];
        
        if ( password != nil )
        {
            // Authentication
            NSString *uuid = [OpenUDID value];
//            NSString *authToken = [QSStrings encodeBase64WithData:[[NSString stringWithFormat:@"%@:%@",uuid, password] dataUsingEncoding:NSUTF8StringEncoding]];
//            NSString *authString = [NSString stringWithFormat:@"Basic %@", authToken];
            
            NSString *authString = [NSString stringWithFormat:@"%@:%@", uuid, [password md5]];
            
            _predefinedHeaders = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                  authString, @"Authorization",
                                  nil];
        }

    

    }
    
    return self;
}


#pragma mark -
#pragma mark 连接并获取数据

/**
 *  开始获取数据！  利用委托
 */
-(void)StartGetData:(NSString *)httpMethod headers:(NSDictionary *)headers body:(NSDictionary *)postBody
{
    // URL
    NSURL *url = [[NSURL alloc] initWithString:
                  [NSString stringWithFormat:@"%@%@",_baseURL, m_apiURL]];
    // Request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    // HTTP Method
    [request setHTTPMethod:httpMethod];
    
    // 设置Headers
    
    // pre defined headers
    if ( _predefinedHeaders != nil )
    {
        for ( NSString *key in _predefinedHeaders )
        {
            [request addValue:[_predefinedHeaders objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    // user defined headers
    if ( headers != nil )
    {
        for ( NSString *key in headers )
        {
            [request addValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    // POST 资料!
    if ( postBody != nil )
    {
        NSMutableString *postString = [[NSMutableString alloc] initWithString:@""];
        
        for ( NSString *key in postBody )
        {
            //[request 
            if ( [postString length] != 0 )
            {
                // 第二个post内容，加&符号
                [postString appendString:@"&"];
            }
            
            [postString appendFormat:@"%@=%@", key, [postBody objectForKey:key]];
        }
        
        // set POST body
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // 网络链接
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if ( connection )
    {
        NSLog(@"Start Connection!");
    }
    else
    {
        NSLog(@"Connection failed...");
    }
}

-(void)StartGetData:(NSString *)httpMethod headers:(NSDictionary *)headers {
    [self StartGetData:httpMethod headers:headers body:nil];
}

-(void)StartGetData:(NSString *)httpMethod 
{
    [self StartGetData:httpMethod headers:nil body:nil];
}

// 默认GET
-(void)StartGetData
{
    [self StartGetData:@"GET" headers:nil];
}

#pragma mark -
#pragma mark Connection委托处理方法

// Response 有反应
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog(@"Get response");
    [m_receivedData setLength:0];
    
}

// 抓取数据
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //NSLog(@"Get data");
    [m_receivedData appendData: data];
}

//- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response{
    
//}

//接收完毕,显示结果
- (void)connectionDidFinishLoading:(NSURLConnection *)aConn {
    
    
    NSString *results = [[NSString alloc]
                         initWithBytes:[m_receivedData bytes]
                         length:[m_receivedData length]
                         encoding:NSUTF8StringEncoding];
    //NSLog(@"%@", results);
    
    // 拆解数据
    id allData = [results objectFromJSONString];  //json parse
    
    
    id innerData = [allData objectForKey:@"data"];
    if (innerData == [NSNull null] ) innerData = nil;
    
    int status = [[allData objectForKey:@"status"] intValue];
    
    NSString *message = [allData objectForKey:@"message"];
    
    // 传递json string
    if ( [_delegate respondsToSelector:@selector(getDataSuccess:)] )
    {
        [_delegate getDataSuccess:results];   // 委托另一个委托
    }
    
    // 获取json dict
    if ( [_delegate respondsToSelector:@selector(getDataSuccess:status:message:)] )
    {
        [_delegate getDataSuccess:innerData status:status message:message];
    }
    
    // 传递model
    if ( [_delegate respondsToSelector:@selector(getModelSuccess:status:message:)] )
    {
        // model自己是否有解析函数
        if ( [self respondsToSelector:@selector(parseModelData:)] )
        {
            // 拆解成model
            if ( innerData != nil ) {
                [self performSelector:@selector(parseModelData:) withObject:innerData];
                [_delegate getModelSuccess:self status:status message:message];    
            }
            else {
                NSLog(@"Inner Data Nil!  Parse Model Data Error");
            }

        }
        else {
            NSLog(@"%@ have no ParseModelData: method", self);
        }

    }
    
} 




// 连接错误
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	
    NSLog(@"Connection failed! Error - %@ %@",	  
          [error localizedDescription],		  
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    
    NSString *errorURL = [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey];
    NSString *errorDescription = [error localizedDescription];
    
    if ( [_delegate respondsToSelector:@selector(getModelError:errorUrl:errorDescription:)] )
    {
        [_delegate getModelError:error errorUrl:errorURL errorDescription:errorDescription];
    }
    
    if ([_delegate respondsToSelector:@selector(getDataError:errorUrl:errorDescription:)] )
    {
        [_delegate getDataError:error errorUrl:errorURL errorDescription:errorDescription];
    }
}
@end
