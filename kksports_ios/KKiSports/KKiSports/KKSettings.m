//
//  KKSettings.m
//  kkjump
//
//  Created by Kelly Mr on 12-3-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKSettings.h"
#import "JSONKit.h"

@implementation KKSettings

@synthesize settingsDict = _settingsDictionary;


static KKSettings *sharedSettings = nil;

+ (KKSettings *)shared
{
    @synchronized(self) {
        if (sharedSettings == nil) {
            sharedSettings = [[super allocWithZone:NULL] init];
            
            // 设置 配置文件
            // 读取配置文件
            
            NSString *path = [[NSBundle mainBundle] pathForResource:SETTINGS_FILE_NAME ofType:SETTINGS_FILE_TYPE];
            
            NSLog(@"Loadidng Setting JSON file: %@", path );
            
            NSData *content = [NSData dataWithContentsOfFile:path];
            

            
            // 从json 读取到字典
            sharedSettings.settingsDict = [content objectFromJSONData];
            

            
            //NSString *path = [[NSBundle mainBundle] pathForResource:SETTINGS_FILE_NAME ofType:SETTINGS_FILE_TYPE];

            // 配置文件放入字典
            //sharedSettings.settingsDict = [[NSDictionary alloc] initWithContentsOfFile:path];  // 哇。这里要用alloc!!! 进行retain增加，否则会异常被释放
            
            
        }
    }
    return sharedSettings;
}

/**
 *  _settingsFile Setter  属性 设置器
 */
//-(void)setSettingsDictionary:(NSDictionary *)settingsDictionary {
//    _settingsDictionary = settingsDictionary;
//}
//
///**
// *  Getter
// */
//-(NSDictionary *)getSettingsDictionary {
//    return _settingsDictionary;
//}

/**
 *  获取设置
 */
+(id)objectForKey:(id)aKey {
    // 获取  array[1] 的值
    return [[[self shared].settingsDict objectForKey:aKey] objectAtIndex:1];
    
}

/*
+ (id)allocWithZone:(NSZone *)zone
{
    return [[self shared] retain];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}

- (void)release
{
    //do nothing
}

- (id)autorelease
{
    return self;
}
*/

//
//static NSDictionary *_settings = nil;
//
//+(NSDictionary *)shared {
//    if ( !_settings ) {
//        NSLog(@"Init settings");
//        // 读取配置文件
//        NSString *path = [[NSBundle mainBundle] pathForResource:SETTINGS_FILE_NAME ofType:SETTINGS_FILE_TYPE];
//        
//        //配置文件放入字典
//        _settings = [NSDictionary dictionaryWithContentsOfFile:path];
//    }
//    
//    NSLog(@"have settings");
//    // 返回配置文件字典
//    return _settings;
//}
//
//
///**
// *  读取设置
// */
//+(id)objectForKey:(id)aKey {
//    return [[self shared] objectForKey:aKey];
//}
//
//
//+ (id)allocWithZone:(NSZone *)zone
//{
//    return [[self shared] retain];
//}
//
//- (id)copyWithZone:(NSZone *)zone
//{
//    return self;
//}
//
//- (id)retain
//{
//    return self;
//}
//
//- (NSUInteger)retainCount
//{
//    return NSUIntegerMax;  //denotes an object that cannot be released
//}
//
//- (void)release
//{
//    //do nothing
//}
//
//- (id)autorelease
//{
//    return self;
//}

@end
