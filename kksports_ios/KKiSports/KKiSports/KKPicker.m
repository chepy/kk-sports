//
//  KKPicker.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKPicker.h"

@implementation KKPicker

-(id)initWithParentView:(UIView *)parentView dataSource:(NSArray *)dataSource delegate:(id)theDelegate {
    
    if ( self = [super init] )
    {
        
        // DataSource 
        _dataSource = dataSource;
        
        // Action Sheet
        CGRect actionSheetframe = CGRectMake(0, 480, 320, 320);
        actionSheet = [[UIActionSheet alloc] initWithFrame:actionSheetframe];
        
        
        // 按钮委托
        if ( theDelegate == nil )
        {
            _delegate = self;
        }
        else {
            _delegate = theDelegate;
        }
        
        // Button  Cancel
        CGRect btnCancelFrame = CGRectMake(10, 5, 60, 30);
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [cancelButton awakeFromNib];
        [cancelButton addTarget:_delegate action:@selector(pickerButtonCancel:)  forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setFrame:btnCancelFrame];
        cancelButton.backgroundColor = [UIColor clearColor];
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [actionSheet addSubview:cancelButton];
        
        // Button OK
        CGRect btnOKFrame = CGRectMake(250, 5, 60, 30);
        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [okButton awakeFromNib];
        [okButton addTarget:_delegate action:@selector(pickerButtonOK:) forControlEvents:UIControlEventTouchUpInside];
        [okButton setFrame:btnOKFrame];
        okButton.backgroundColor = [UIColor clearColor];
        [okButton setTitle:@"完成" forState:UIControlStateNormal];
        [actionSheet addSubview:okButton];
        
        // Picker View
        CGRect pickerFrame = CGRectMake(0, 40, 320, 400);
        picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
        picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        picker.showsSelectionIndicator = YES; 
        picker.delegate=self; 
        picker.dataSource=self;
        picker.tag = 101; 
        picker.hidden = NO;
        [actionSheet addSubview: picker];
        
        // 默认选中
        _selectedRow = 0;
        _selectedComponent = 0;
        
        
        // Add To Parent!!
        _parentView = parentView;
        [_parentView addSubview:actionSheet];
        
    }
    return self;
}

-(id)initWithParentView:(UIView *)parentView dataSource:(NSArray *)dataSource {
    
    return [self initWithParentView:parentView dataSource:(NSArray *)dataSource delegate:self];
}

-(void)show {
    
    if ( !isPickerShow )
    {
        CGRect pickerFrame = actionSheet.frame;
		[UIView beginAnimations:nil context:(void *)self];  // context?

		[UIView setAnimationDelegate:self];
		[UIView setAnimationDuration:0.4f];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
		[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:[actionSheet superview] cache:YES];	
		pickerFrame.origin.y -= pickerFrame.size.height - 40;//应该根据应用动态计算需要的位置
		[actionSheet setFrame:pickerFrame];	
		[UIView commitAnimations];	
		isPickerShow = TRUE;
    }
    
}

-(void)hide {
	if(isPickerShow)
	{
		CGRect pickerFrame = actionSheet.frame;	
		[UIView beginAnimations:nil context:(void *)self];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDuration:0.4f];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
		[UIView setAnimationTransition:UIViewAnimationTransitionNone forView:[actionSheet superview] cache:YES];	
		pickerFrame.origin.y += pickerFrame.size.height - 40;//应该根据应用动态计算需要的位置
		[actionSheet setFrame:pickerFrame];	
		[UIView commitAnimations];	
		isPickerShow = FALSE;
	}	
}
-(void)selectRow:(NSInteger)row inComponent:(NSInteger)component {
    [picker selectRow:row inComponent:component animated:YES];

}
-(NSInteger)selectedRowInComponent:(NSInteger)component {
    return [picker selectedRowInComponent:component];
}
    
#pragma mark -
#pragma mark KKPicker默认的委托
-(void)pickerButtonOK:(id)sender
{
    [self hide];
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"按下完成" message:@"Picker的完成键" delegate:nil cancelButtonTitle:@"嗯" otherButtonTitles:nil];
    [alert show];
}
-(void)pickerButtonCancel:(id)sender
{
    [self hide];
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"按下取消" message:@"Picker的取消键" delegate:nil cancelButtonTitle:@"嗯" otherButtonTitles:nil];
    [alert show];
}


#pragma mark -
#pragma mark UIPickerView选择器委托
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
			forComponent:(NSInteger)component
{
	//return [self.sourceArray objectAtIndex:row];
    return [[_dataSource objectAtIndex:component] objectAtIndex:row];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return [_dataSource count];
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[_dataSource objectAtIndex:component] count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    _selectedComponent = &component;
    _selectedRow = &row;
    
    // 委托
    if ( _delegate != self )
    {
        SEL delegateSel = @selector(pickerView:didSelectRow:inComponent:);
        if ( [_delegate respondsToSelector:delegateSel] )
        {

            NSMethodSignature *sig = [_delegate methodSignatureForSelector:delegateSel];
            
            if ( sig )
            {
                NSInvocation *invo = [NSInvocation invocationWithMethodSignature:sig];

                [invo setTarget:_delegate];
                [invo setSelector:delegateSel];
                [invo setArgument:&pickerView atIndex:2];
                [invo setArgument:&row atIndex:3];
                [invo setArgument:&component atIndex:4];
                
                [invo invoke];
//                if (sig.methodReturnLength) { 
//                    id anObject; 
//                    [invo getReturnValue:&anObject]; 
//                    NSLog(@"Delegate here success");
//                } else { 
//                    NSLog(@"Delegate PROBLEM");
//                } 
            } else { 
                NSLog(@"Delegate PROBLEM");
            }
            
        }
    }
    
}

@end
