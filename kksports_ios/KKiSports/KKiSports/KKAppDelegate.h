//
//  KKAppDelegate.h
//  KKiSports
//
//  Created by Kelly Mr on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface KKAppDelegate : UIResponder <UIApplicationDelegate>
{
    //NSMutableData *receivedData;
}

@property (strong, nonatomic) UIWindow *window;

@end
