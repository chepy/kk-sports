//
//  FindPeopleViewController.h
//  KKiSports
//
//  Created by Kelly Mr on 12-5-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FindPeopleViewController : UITableViewController
<CLLocationManagerDelegate
,UIActionSheetDelegate 
>

{
    IBOutlet UIBarButtonItem *nearButton;
    
    NSMutableArray *m_peopleDataSource;  // 存放获取用户的数据源
    
    CLLocationManager *m_locationManager;
}

@property (retain, nonatomic) NSMutableArray *peopleDataSource;

@end
