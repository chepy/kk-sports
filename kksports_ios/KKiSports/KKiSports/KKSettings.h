//
//  KKSettings.h
//  kkjump
//
//  Created by Kelly Mr on 12-3-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
//  读取游戏核心 plist 配置文件



#define SETTINGS_FILE_NAME @"GameSettings"
#define SETTINGS_FILE_TYPE @"json"


@interface KKSettings : NSObject {
    
    NSDictionary *_settingsDictionary;
    
}

@property (nonatomic, retain) NSDictionary *settingsDict;

// 获取设置
+(id)objectForKey:(id)aKey;



+ (KKSettings *)shared;

// Setter
//-(void)setSettingsDictionary:(NSDictionary *)settingsDictionary;
//
//// Getter
//-(NSDictionary *)getSettingsDictionary;


//-(void)release;

@end
