//
//  FindPeopleViewController.m
//  KKiSports
//
//  Created by Kelly Mr on 12-5-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FindPeopleViewController.h"
#import "Models.h"
#import "OpenUDID.h"
#import "MainTabBarViewController.h"

#import "NearResponser.h"
#import "PersonCell.h"

@interface FindPeopleViewController ()

@end

@implementation FindPeopleViewController

@synthesize peopleDataSource = m_peopleDataSource;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 给点虚拟数据
    m_peopleDataSource = [[NSMutableArray alloc] init];
    
    
    //NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:"123", nil
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"刷新附近" style:UIBarButtonItemStylePlain target:self action:@selector(OnClickNear:)];
    nearButton = self.navigationItem.leftBarButtonItem;  // Bind
    
#ifdef DEBUG
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"调试" style:UIBarButtonItemStylePlain target:self action:@selector(OnClickDebug)];
    
#endif
}

// 刷新查看附近的人
-(void)OnClickNear:(UIBarButtonItem *)sender
{

    // 获取地理位置
    m_locationManager = [[CLLocationManager alloc]init]; 
    
    m_locationManager.delegate = self;
    m_locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    m_locationManager.distanceFilter = 1000.0f;
    [m_locationManager startUpdatingLocation];

    
}

// Location after, 执行查看附近的人
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [nearButton setEnabled:NO];
    [nearButton setTitle:@"请稍候..."];
    
    // Loading show
    MainTabBarViewController *m = (MainTabBarViewController *)self.tabBarController;
    [m.loadingIndicator startAnimating];
    
    // 开始读取附近的人
    NearResponser *responser = [[NearResponser alloc] initWithController:self];
    [NearModel doNearWithLatitude:newLocation.coordinate.latitude longtitude:newLocation.coordinate.longitude delegate:responser];
    
    // stop
    [m_locationManager stopUpdatingLocation];
    m_locationManager = nil;
    
}

// Location Error
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

#ifdef DEBUG
-(void)OnClickDebug
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"调试窗口" delegate:self cancelButtonTitle:@"返回" destructiveButtonTitle:nil otherButtonTitles:@"获取用户信息", @"查看预存密码",@"更多", nil];
    
    [actionSheet showInView:self.parentViewController.tabBarController.view];
}

// 调试菜单
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        
        
        UserModel *userModel = [[UserModel alloc] initWithDelegate:self];
        [userModel StartGetData:@"GET"];
        
        // show loading
        MainTabBarViewController *tbc = (MainTabBarViewController *)self.tabBarController;
        [tbc.loadingIndicator startAnimating];

    }
    
    // 查看密码
    if (buttonIndex == 1)
    {
        UIAlertView *someError = [[UIAlertView alloc] initWithTitle: @"Saved Password" message: [[NSUserDefaults standardUserDefaults] objectForKey:@"savedPassword"] delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        
        [someError show];
    }
    
    // 更多调试内容
    if (buttonIndex == 2 )
    {
        [self performSegueWithIdentifier:@"DebugWindow" sender:self];
    }
    
}
-(void)getDataSuccess:(NSString *)result {
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle:@"获取用户信息" 
                          message:result//[NSString stringWithFormat:@"%d", buttonIndex]
                          delegate:self 
                          cancelButtonTitle:@"OK!" 
                          otherButtonTitles:nil];
    [alert show];
    
    MainTabBarViewController *tbc = (MainTabBarViewController *)self.tabBarController;
    [tbc.loadingIndicator stopAnimating];
}

-(void)getDataError:(NSError *)error errorUrl:(NSString *)url errorDescription:(NSString *)description
{
    NSLog(@"%@", description);
}
#endif




- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    //return 5;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [m_peopleDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PersonCell";
    PersonCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];    
    
    
    // Configure the cell...
    NSDictionary *personDict = [m_peopleDataSource objectAtIndex:indexPath.row];
    NSString *s = [personDict objectForKey:@"nickname"];
    
    cell.labelNickname.text = s;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
