//
//  KKAsyncImageView.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KKAsyncImageView : UIView {
    NSURLConnection* connection;
    NSMutableData* data;
}
@end
