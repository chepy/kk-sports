//
//  SettingProfileViewController.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SettingProfileViewController.h"

#import "SettingModel.h"

@interface SettingProfileViewController ()

@end

@implementation SettingProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    genderDataSource = [[NSArray alloc] initWithObjects:@"秘密", @"男", @"女", nil];
    pickerDataSource = [[NSArray alloc] initWithObjects:genderDataSource, nil];
    genderPicker = [[KKPicker alloc] initWithParentView:self.tabBarController.view dataSource:pickerDataSource delegate:self];
    
    // 读取预存的资料到屏幕
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *savedGender = [ud objectForKey:@"profileGender"];
    
    // 性别
    if ( savedGender )
    {
        profileGender.text = savedGender;
    }
    
    // 昵称
    NSString *savedNickname = [ud objectForKey:@"profileNickname"];
    if ( savedNickname )
    {
        profileName.text = savedNickname;
    }
    
}

-(IBAction)settingAvatar:(id)sender
{
    UIActionSheet *choosePhotoActionSheet;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        choosePhotoActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"choose_photo", @"")
                                                             delegate:self 
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", @"") 
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"take_photo_from_camera", @""), NSLocalizedString(@"take_photo_from_library", @""), nil];
    } else {
        choosePhotoActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"choose_photo", @"")
                                                             delegate:self 
                                                    cancelButtonTitle:NSLocalizedString(@"cancel", @"") 
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"take_photo_from_library", @""), nil];
    }
    
    [choosePhotoActionSheet showInView:self.view];
}

-(IBAction)settingGender:(id)sender
{

    //[self.parentViewController.tabBarController.view addSubview:genderPicker];

    //NSLog(@"OK");
    // 默认选中 当前 label 记录！
    for (int i; i < genderDataSource.count; i++) {
        if ( profileGender.text == [genderDataSource objectAtIndex:i] )
        {
            [genderPicker selectRow:i inComponent:0];
        }
    }
    
    
    [genderPicker show];
}

-(IBAction)saveProfile:(id)sender
{
    // UserDefaults ....
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:profileName.text forKey:@"profileNickname"];
    [ud setObject:profileGender.text forKey:@"profileGender"];
    
    // Remote API
    UserModel *editUser = [[UserModel alloc] init];
    editUser.nickname = profileName.text;
    
    // 性别判断
    for(int key=0; key < 2; key++)
    {
        if ( profileGender.text == [genderDataSource objectAtIndex:key] )
        {
            editUser.gender = [[NSNumber alloc] initWithInt:key];
        }
    }
//    if ( profileGender.text == @"男" )
//    {
//        editUser.gender = MALE;
//    }
//    else if ( profileGender.text == @"女" ) {
//        editUser.gender = FEMALE;
//    }
//    else if ( profileGender.text == @"秘密" )
//    {
//        editUser.gender = SECRET_GENDER;
//    }
    
    
    [SettingModel doSettingWithUserModel:editUser delegate:nil];
    
    NSLog(@"Save!");
    [self.navigationController popViewControllerAnimated:YES];
    

}

- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"Will Disappear!");
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark 性别Picker处理方法
-(void)pickerButtonOK:(id)sender
{
    //[genderPicker 
    // 获取选择的值
    NSString *genderVal = [[pickerDataSource objectAtIndex:0] objectAtIndex: [genderPicker selectedRowInComponent:0]];
    profileGender.text= genderVal;
    [genderPicker hide];
}
-(void)pickerButtonCancel:(id)sender{
   [genderPicker hide];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    profileGender.text = [[pickerDataSource objectAtIndex:component] objectAtIndex:row];
}

// 键盘
-(IBAction)textFieldDoneEditing:(id)sender
{
    [sender resignFirstResponder];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSUInteger sourceType = 0;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        switch (buttonIndex) {
            case 0:
                sourceType = UIImagePickerControllerSourceTypeCamera;
                break;
            case 1:
                sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                break;
            case 2:
                return;
        }
    } else {
        if (buttonIndex == 1) {
            return;
        } else {
            sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
    }
    
	UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
	imagePickerController.delegate = self;
	imagePickerController.allowsEditing = YES;
    imagePickerController.sourceType = sourceType;
	[self presentModalViewController:imagePickerController animated:YES];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info 
{
	[picker dismissModalViewControllerAnimated:YES];
    NSLog(@"Display photo");
	//self.photo = [info objectForKey:UIImagePickerControllerEditedImage];
	//[self.photoButton setImage:self.photo forState:UIControlStateNormal];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissModalViewControllerAnimated:YES];
}

//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    // Configure the cell...
//    
//    return cell;
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

//#pragma mark - Table view delegate
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Navigation logic may go here. Create and push another view controller.
//    /*
//     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     */
//}


@end
