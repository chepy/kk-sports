//
//  KKLocation.m
//  KKiSports
//
//  Created by Kelly Mr on 12-4-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKLocation.h"

@implementation KKLocation

static KKLocation *sharedInstance = nil;

+ (KKLocation *)shared
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[super allocWithZone:NULL] init];
            
            //[sharedInstance setBigDict:[ [NSMutableDictionary alloc] init]]; 
        }
    }
    return sharedInstance;
}


-(CLLocation *)getLocation
{
    [[KKLocation shared] startRead];
    
    CLLocation *theLocation = m_location;
    
    if ( !m_location )
    {
        return [self getLocation];
        
    }
    
    //[[KKLocation shared] stopRead];
    
    return theLocation;
}

-(void)startRead
{
    m_manager = [[CLLocationManager alloc] init];
    m_manager.delegate = self;
    m_manager.desiredAccuracy = kCLLocationAccuracyBest;
    m_manager.distanceFilter = kCLDistanceFilterNone;
    
    NSLog(@"Core Location Reading...");
    [m_manager startUpdatingLocation];
    
}

-(void)stopRead
{
    NSLog(@"Core Location Stopping...");
    [m_manager stopUpdatingLocation];
    
    //[m_manager release];
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    m_oldLocation = oldLocation;
    m_location = newLocation;
    
    NSLog(@"Latitude: %+.6f", newLocation.coordinate.latitude );
}

@end
