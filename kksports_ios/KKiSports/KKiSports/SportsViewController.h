//
//  KKSportsViewController.h
//  KKiSports
//
//  Created by Kelly Mr on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportsViewController : UITableViewController {
    
    //NSDictionary *_dataSource;
    NSArray *_sportsList;
    
}

// 运动列表
@property (nonatomic, retain) NSArray *sportsList;

@end
