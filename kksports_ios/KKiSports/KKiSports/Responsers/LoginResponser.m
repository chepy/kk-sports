//
//  LoginResponser.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LoginResponser.h"
#import "LoginModel.h"

//主控制器, Tab Bar
#import "MainTabBarViewController.h"  

// 登录处理
@implementation LoginResponser


#pragma -
#pragma 登录 委托 处理
-(void)getModelSuccess:(LoginModel *)model status:(int)status message:(NSString *)message
{
    if ( status == 1 )
    {
        // 设置密码
        if ([model.auth isEqualToString:@"1"])
        {
            NSLog(@"New UUID Register!: %@", message);
            [[NSUserDefaults standardUserDefaults] setObject:model.createdPassword forKey:@"savedPassword"];
        }
    }
    else {
        NSLog(@"Login Responser : %@", message);
    }
    
    // 隐藏loading
    [((MainTabBarViewController *)self.controller).loadingIndicator stopAnimating];
}

-(void)getModelError:(NSError *)error errorUrl:(NSString *)url errorDescription:(NSString *)description{
    
    NSLog(@"Login Network Error");
}

@end
