//
//  KKResponser.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKSingleton.h"
#import "KKModel.h"

@interface KKResponser : NSObject
{
    id _controller;
}

@property (retain, nonatomic) id controller;

-(id)initWithController:(id)theController;

@end
