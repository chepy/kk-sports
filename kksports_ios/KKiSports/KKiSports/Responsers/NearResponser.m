//
//  NearResponser.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NearModel.h"
#import "NearResponser.h"
#import "MainTabBarViewController.h"
#import "FindPeopleViewController.h"

@implementation NearResponser

#pragma -
#pragma 登录 委托 处理
-(void)getModelSuccess:(NearModel *)model status:(int)status message:(NSString *)message
{
    if (status == 1)
    {
        NSLog(@"Near Resonser!");
        if ( self.controller != nil )
        {
            FindPeopleViewController *c = (FindPeopleViewController *)self.controller;
            [c.navigationItem.leftBarButtonItem setEnabled:YES]; // 恢复可用
            [c.navigationItem.leftBarButtonItem setTitle:@"刷新附近"]; // 恢复可用
            
            // loading  stop
            MainTabBarViewController *m = (MainTabBarViewController *)c.tabBarController;
            [m.loadingIndicator stopAnimating];

        }
        
        NSLog(@"Message: %@", message);
//        
//        NSLog(@"First User UUID: %@", [[model.users objectAtIndex:0] objectForKey:@"uuid"]);
        
        // 更新数据源， 更新显示
        FindPeopleViewController *controller = (FindPeopleViewController *)self.controller;
        controller.peopleDataSource = (NSMutableArray *)model.users;
        [controller.tableView reloadData];
    }
    else {
        NSLog(@"Near Resonser Error");
    }
}

-(void)getModelError:(NSError *)error errorUrl:(NSString *)url errorDescription:(NSString *)description{
    NSLog(@"Near Model Get Error");
}
@end
