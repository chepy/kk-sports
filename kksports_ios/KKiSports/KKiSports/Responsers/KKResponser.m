//
//  KKResponser.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKResponser.h"

@implementation KKResponser

@synthesize controller = _controller;


-(id)initWithController:(id)theController
{
    if (self = [super init])
    {
        _controller = theController;
    
    }
    
    return self;
}

@end
