//
//  SettingModel.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface SettingModel : KKModel <KKModelProtocol> {
    // 只返回是否成功，因此不设model内容
}


+(void)doSettingWithUserModel:(UserModel *)userModel delegate:(id)theDelegate;

@end
