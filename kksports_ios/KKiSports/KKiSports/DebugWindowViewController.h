//
//  DebugWindowViewController.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebugWindowViewController : UITableViewController {
    
    
    IBOutlet UITextField *apiUrlField;
    
}

-(IBAction)saveAction:(id)sender;

@end
