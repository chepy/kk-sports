//
//  NearModel.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKModel.h"

@interface NearModel : KKModel <KKModelProtocol> {
    
@public
    NSArray *users;  // 获得用户
}
@property (nonatomic, retain) NSArray *users;

+(void)doNearWithLatitude:(float)theLatitude longtitude:(float)theLongitude delegate:(id)theDelegate;

@end
