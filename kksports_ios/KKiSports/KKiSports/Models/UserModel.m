//
//  UserModel.m
//  KKiSports
//
//  Created by Kelly Mr on 12-5-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UserModel.h"
#import "KKSettings.h"

@implementation UserModel

@synthesize uuid;
@synthesize username;
@synthesize gender;
@synthesize nickname;
@synthesize password;
@synthesize recentLatitude;
@synthesize recentLongitude;

-(id)initWithDelegate:(id)delegate
{
    if ( self = [super init] )
    {
        m_apiURL = [m_apiSettings objectForKey:@"user"]; 
        _delegate = delegate;
    }
    
    return self;
}

// 模型 <-> JSON对象  解析
-(void)parseModelData:(NSDictionary *)data
{
    
    uuid = [data objectForKey:@"uuid"];
    username = [data objectForKey:@"username"];
    recentLatitude = [data objectForKey:@"recentLatitude"];
    recentLongitude = [data objectForKey:@"recentLogitude"];
    
    //createdPassword = [data objectForKey:@"newPassword"];
}

-(void)setApiWithUUID:(NSString *)theUUID
{
    m_apiURL = [NSString stringWithFormat:@"%@/%@", m_apiURL, theUUID];  // + UUID
}

@end
