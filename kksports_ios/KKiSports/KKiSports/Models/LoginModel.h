//
//  LoginModel.h
//  KKiSports
//
//  Created by Kelly Mr on 12-5-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKModel.h"

@interface LoginModel : KKModel <KKModelProtocol> {
    
@public
    NSString *auth;  // 是否登录成功

    NSString *uuid;
    NSString *createdPassword;  // 返回新的密码，用于保存的设备    
}
@property (nonatomic, retain) NSString *uuid;
@property (retain, nonatomic ) NSString *createdPassword;
@property (retain, nonatomic) NSString *auth;

// 执行注册
+(void)doRegisterWithUUID:(NSString *)theUUID delegate:(id)theDelegate;

@end
