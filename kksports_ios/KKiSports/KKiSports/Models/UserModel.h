//
//  UserModel.h
//  KKiSports
//
//  Created by Kelly Mr on 12-5-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKModel.h"

// 性别
//typedef enum 
//{
//    
//    SECRET_GENDER=0,
//    MALE=1,
//    FEMALE=2
//    
//} GENDER;


@interface UserModel : KKModel <KKModelProtocol> {

@public

    NSString *uuid;
    NSString *username;
    NSString *nickname;
    
    NSString *password;

    NSNumber *gender;
    
    NSNumber *recentLatitude;

    NSNumber *recentLongitude;
    
}

@property (retain, nonatomic) NSString *uuid;
@property (retain, nonatomic) NSString *username;
@property (retain, nonatomic) NSString *nickname;
@property (retain, nonatomic) NSNumber *gender;
@property (retain, nonatomic) NSString *password;
@property (readwrite, nonatomic) NSNumber *recentLatitude;
@property (readwrite, nonatomic) NSNumber *recentLongitude;

// 设置UUID API
-(void)setApiWithUUID:(NSString *)uuid;

@end
