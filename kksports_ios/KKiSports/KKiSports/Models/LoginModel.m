//
//  LoginModel.m
//  KKiSports
//
//  Created by Kelly Mr on 12-5-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LoginModel.h"
#import "KKSettings.h"

@implementation LoginModel


@synthesize uuid, createdPassword, auth;

-(id)initWithDelegate:(id)delegate
{
    if ( self = [super init] )
    {
        m_apiURL = [m_apiSettings objectForKey:@"login"]; 
        _delegate = delegate;
    }
    
    return self;
}

// 模型 <-> JSON对象  解析
-(void)parseModelData:(NSDictionary *)data
{
    uuid = [data objectForKey:@"uuid"];
    createdPassword = [data objectForKey:@"newPassword"];
    auth = [data objectForKey:@"auth"];
    
}


+(void)doRegisterWithUUID:(NSString *)theUUID delegate:(id)theDelegate
{
    LoginModel *loginModel = [[LoginModel alloc] initWithDelegate:theDelegate];
    
    
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              theUUID, @"uuid",nil];
    
    [loginModel StartGetData:@"POST" headers:nil body:postData];
}

@end
