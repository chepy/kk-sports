//
//  NearModel.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NearModel.h"

@implementation NearModel

@synthesize users;

-(id)initWithDelegate:(id)delegate
{
    if ( self = [super init] )
    {
        m_apiURL = [m_apiSettings objectForKey:@"near"]; 
        _delegate = delegate;
    }
    
    return self;
}

-(void)parseModelData:(NSArray *)data {
    // 读取到的users...
    users = data;
}

+(void)doNearWithLatitude:(float)theLatitude longtitude:(float)theLongitude delegate:(id)theDelegate
{
    NearModel *nearModel = [[NearModel alloc] initWithDelegate:theDelegate];
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    // 传递 经纬度
    [postData setObject:[[NSNumber alloc] initWithFloat:theLatitude] forKey:@"latitude"];
    [postData setObject:[[NSNumber alloc] initWithFloat:theLongitude] forKey:@"longitude"];
    
    [nearModel StartGetData:@"POST" headers:nil body:postData];
    
}

@end
