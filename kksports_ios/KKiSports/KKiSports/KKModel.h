//
//  KKModel.h
//  KKiSports
//
//  Created by Kelly Mr on 12-5-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


// Model 继承协议
@protocol KKModelProtocol <NSObject>
@required
-(id)initWithDelegate:(id)delegate; // 传入委托的初始化
-(void)parseModelData:(NSDictionary *)data; // 将json 转化成 model object  可NSArray
@end


// Model 委托 协议
@protocol KKModelDelegate <NSObject>


@optional

// Data
-(void)getDataSuccess:(NSString *)result;  // 成功获取数据后。.....
-(void)getDataSuccess:(NSDictionary *)data status:(int)status message:(NSString *)message;
-(void)getDataError:(NSError *)error errorUrl:(NSString *)url errorDescription:(NSString *)description;

// Model
-(void)getModelSuccess:(id)model status:(int)status message:(NSString *)message;
-(void)getModelError:(NSError *)error errorUrl:(NSString *)url errorDescription:(NSString *)description;

@end



// Abstract Model
@interface KKModel : NSObject 
{
    
    id _delegate; // <KKModelDelegate>
    
    NSString *_baseURL;
    
    NSString *m_apiURL;
    NSMutableData *m_receivedData;
    
    NSMutableDictionary *m_apiSettings;  // API设置文件
    
    NSMutableDictionary *_predefinedHeaders; // 预定义的Headers
    
    
}
-(void)StartGetData:(NSString *)httpMethod headers:(NSDictionary *)headers body:(NSDictionary *)postBody;
-(void)StartGetData:(NSString *)httpMethod headers:(NSDictionary *)headers;
-(void)StartGetData:(NSString *)httpMethod;
-(void)StartGetData;

@end


