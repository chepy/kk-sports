//
//  MainTabBarViewController.m
//  KKiSports
//
//  Created by Kelly Mr on 12-5-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MainTabBarViewController.h"
//#import "KKAppKit.h"
#import "Models.h"
#import "OpenUDID.h"
#import "LoginResponser.h"

@interface MainTabBarViewController ()

@end

@implementation MainTabBarViewController

@synthesize loadingIndicator = loadingIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated
{
    
    
    // 初始化Loading 标志...  确定位置
    loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    
    [loadingIndicator setCenter:CGPointMake(160.0f, 208.f)];
    [loadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    
    [self.view addSubview:loadingIndicator];
    //[loadingIndicator startAnimating];
    
    
    
    // 程序开始时，登录！
    
    // 获取记录的用户名和密码
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *uuid = [OpenUDID value];
    NSString *savedPassword = [userDefaults objectForKey:@"savedPassword"];
    if ( savedPassword == nil )
    {
        // 没有登录过，开始注册
        
//        LoginModel *loginModel = [[LoginModel alloc] initWithDelegate:self];
//        NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                  uuid, @"uuid",nil];
//        [loginModel StartGetData:@"POST" headers:nil body:postData];
        // 显示loading
        [LoginModel doRegisterWithUUID:uuid delegate:[[LoginResponser alloc] initWithController:self]];
        
    }
    
}
#pragma -
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
