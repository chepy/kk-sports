//
//  MainTabBarViewController.h
//  KKiSports
//
//  Created by Kelly Mr on 12-5-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabBarViewController : UITabBarController {
    
    IBOutlet UIActivityIndicatorView *loadingIndicator;
    
}

@property (retain, nonatomic) UIActivityIndicatorView *loadingIndicator;

@end
