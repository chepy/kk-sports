//
//  SettingProfileViewController.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKPicker.h"

@interface SettingProfileViewController : UITableViewController
<UIImagePickerControllerDelegate, UIActionSheetDelegate,
UINavigationControllerDelegate> 
{
//    IBOutlet UIPickerView *genderPicker;
    
    KKPicker *genderPicker;
    NSArray *genderDataSource;
    NSArray *pickerDataSource;
    
    IBOutlet UITextField *profileName;
    IBOutlet UILabel *profileGender;
    
}

-(IBAction)settingGender:(id)sender;

-(IBAction)settingAvatar:(id)sender;

-(IBAction)textFieldDoneEditing:(id)sender;


-(IBAction)saveProfile:(id)sender;  // 保存资料
@end
