//
//  KKLocation.h
//  KKiSports
//
//  Created by Kelly Mr on 12-4-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface KKLocation : NSObject <CLLocationManagerDelegate>{
    CLLocationManager *m_manager;
    CLLocation *m_oldLocation;
    CLLocation *m_location;
}

+ (KKLocation *)shared; // 单例

/**
 * 获取当前位置
 */
-(CLLocation *)getLocation;

-(void)startRead;
-(void)stopRead;

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;

@end
