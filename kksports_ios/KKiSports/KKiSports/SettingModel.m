//
//  SettingModel.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SettingModel.h"


@implementation SettingModel

-(id)initWithDelegate:(id)delegate
{
    if ( self = [super init] )
    {
        m_apiURL = [m_apiSettings objectForKey:@"setting"]; 
        _delegate = delegate;
    }
    
    return self;
}

// 模型 <-> JSON对象  解析
-(void)parseModelData:(NSDictionary *)data
{
    
}

// 传入UserModel, 进行User设置
+(void)doSettingWithUserModel:(UserModel *)userModel delegate:(id)theDelegate
{
    SettingModel *settingModel = [[SettingModel alloc] initWithDelegate:theDelegate];
    
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    
    if ( userModel.nickname != nil )
    {
        [postData setObject:userModel.nickname forKey:@"nickname"];
    }
    
    if ( userModel.gender != nil )
    {
//        NSNumber *genderNum = [[NSNumber alloc] initWithInteger:(NSUInteger)userModel.gender];
        [postData setObject:userModel.gender  forKey:@"gender"];
    }
    
//    userModel.gender = GENDE
    // 其它要修改的属性  TODO
//    if ( userModel.username ) 
    
    [settingModel StartGetData:@"POST" headers:nil body:postData];
}
@end
