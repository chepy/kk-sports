//
//  KKBox.m
//  kkjump
//
//  Created by Mrkelly on 12-3-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKCup.h"



@implementation KKCup

static KKCup *sharedCup = nil;

+ (KKCup *)shared
{
    @synchronized(self) {
        if (sharedCup == nil) {
            sharedCup = [[super allocWithZone:NULL] init];
            
            [sharedCup setBigDict:[ [NSMutableDictionary alloc] init]]; 
        }
    }
    return sharedCup;
}

/**
 *  _settingsFile Setter  属性 设置器
 */
-(void)setBigDict:(NSMutableDictionary *)bigDict {
    _bigDict = bigDict;
}

/**
 *  Getter
 */
-(NSMutableDictionary *)getBigDict {
    return _bigDict;
}

/**
 *  获取设置
 */
+(id)objectForKey:(id)aKey {
    //[[self shared] getSettingsDictionary];
    return [[[self shared] getBigDict] objectForKey:aKey];
    
}

// 设置
+ (void)setObject:(id)anObject forKey:(id)aKey {
    [[[self shared] getBigDict] setObject:anObject forKey:aKey];
}



@end
