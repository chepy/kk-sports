//
//  KKSettings.h
//  kkjump
//
//  Created by Kelly Mr on 12-3-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
//  读取游戏核心 plist 配置文件





@interface KKStore : NSObject {
    
    NSDictionary *_settingsDictionary;
    
}

@property (nonatomic, retain) NSDictionary *settingsDict;

// 获取设置
+(id)objectForKey:(id)aKey;



+ (KKStore *)shared;


@end
