//
//  KKSingleton.m
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKSingleton.h"

@implementation KKSingleton

static id instance;

+(id)shared
{
    @synchronized(self)
    {
        if ( !instance)
        {
            instance = [[self alloc] init];
        }
    }
    
    return instance;
}

@end
