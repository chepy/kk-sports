//
//  KKSingleton.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KKSingleton : NSObject

+(id)shared;

@end
