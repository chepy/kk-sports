//
//  KKSportsViewController.m
//  KKiSports
//
//  Created by Kelly Mr on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SportsViewController.h"
#import "KKAppKit.h"

@implementation SportsViewController

@synthesize sportsList = _sportsList;

- (void)viewDidLoad {  

    [super viewDidLoad];
    
    // 读取数据
    self.sportsList = [KKStore objectForKey:@"sports_list"]; // 运动列表
}


#pragma mark 表格数据源

// 数据行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.sportsList count];
}

// 单元格内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sport_cell"];  // 获得复用单元格模板
    
    cell.textLabel.text = [[self.sportsList objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text = @"有xx人";

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // 设置全局参数, 让下一个push的controller接收到改变而改变

}


@end
