//
//  KKSettings.m
//  kkjump
//
//  Created by Kelly Mr on 12-3-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "KKStore.h"
#import "JSONKit.h"

@implementation KKStore

@synthesize settingsDict = _settingsDictionary;


static KKStore *sharedSettings = nil;

+ (KKStore *)shared
{
    @synchronized(self) {
        if (sharedSettings == nil) {
            sharedSettings = [[super allocWithZone:NULL] init];
            
            // 设置 配置文件
            // 读取配置文件
            
            NSString *path = [[NSBundle mainBundle] pathForResource:@"AppData" ofType:@"json"];
            
            NSLog(@"Loadidng Setting JSON file: %@", path );
            
            NSData *content = [NSData dataWithContentsOfFile:path];
            

            
            // 从json 读取到字典
            sharedSettings.settingsDict = [content objectFromJSONData];
            
            
        }
    }
    return sharedSettings;
}


/**
 *  获取设置
 */
+(id)objectForKey:(id)aKey {
    return [[self shared].settingsDict objectForKey:aKey];
    
}


@end
