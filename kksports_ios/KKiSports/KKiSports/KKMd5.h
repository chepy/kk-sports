//
//  KKMd5.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//


@interface NSString (KKMd5)
- (NSString *) md5;
@end

@interface NSData (KKMd5)
- (NSString*)md5;
@end
