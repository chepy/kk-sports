//
//  PersonCell.h
//  KKiSports
//
//  Created by Kelly Mr on 12-6-22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCell : UITableViewCell
{
    IBOutlet UILabel *labelNickname;
}
@property (retain, nonatomic) UILabel *labelNickname;

@end
