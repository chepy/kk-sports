module.exports = (app)->

  # ORM framework
  app.DB = require('sequelize')

  # DB INIT WITH Database Config...
  app.db_instance = new app.DB(
    app.CONFIG.DB_NAME,
    app.CONFIG.DB_USERNAME,
    app.CONFIG.DB_PASSWORD,
    {
      host: app.CONFIG.DB_HOST,
      port: app.CONFIG.DB_PORT
    }
  )


  # 返回数据模型
  return {
    User: require('./User')( app )
  }

