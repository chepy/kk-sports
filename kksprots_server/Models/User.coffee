module.exports = ( app )->
  return app.db_instance.define(
    app.CONFIG.DBTABLE_USER,
    {
      # 用户设备ID
      uuid: {
        primaryKey:true,
        type: app.DB.STRING
      },
      # 用户登录名
      username : {
        type: app.DB.STRING
      },
      # 用户名字
      nickname : {
        type: app.DB.STRING
      },
      # 密码
      password : app.DB.STRING,
      
      # 最近一次的纬度
      recentLatitude : {
        type: app.DB.FLOAT
      },
      # 最近一次经度
      recentLongitude : {
        type: app.DB.FLOAT
      }
      
    },
    {
      freezeTableName: true
    }
  )