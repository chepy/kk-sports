# 服务器初始化，并配置
global.app = require('./init')

# 定义常用变量
app.loginUUID = null  # 如登录，会被设置成登录用户ID


# 一些辅助性函数
global.helper = {}

# 返回的json格式定义，常用
global.write_api = (response, status, message, data)->
  response.json {
    status : status,
    message : message,
    data : data
  }
  return;

#控制器，数据模型
app.controllers = require('./Controllers')( app )
app.models = require('./Models')( app )

app.listen 3000, ->
  console.log("KKSports server listening on port %d in %s mode", app.address().port, app.settings.env)

