module.exports = (request, response)->
  #user_id = app.loginUUID
  post_data = request.body  # 获取所有post的数据
  post_data.uuid = app.loginUUID
  
  # 开始设置当前用户!
  app.models.User.find({
    where: {
      uuid : app.loginUUID
    }
  }).success((user)->
    # 更新
    user.updateAttributes(post_data).success(()->
      return write_api( response, true, "用户设置成功!", null )
    ).error((errors)->
      return write_api(response, false, "用户设置失败!" + errors, null)
    )
  ).error((errors)->
    console.log "error"
    return write_api( response, false, "错误更新用户", errors )
  )