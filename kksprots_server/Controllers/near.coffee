module.exports = (request, response)->
  # 获取附近的用户
  latitude = request.body.latitude
  longitude = request.body.longitude
  
  # 更新用户位置信息
  helper.updateUserPosition( latitude, longitude, response, (res)->
    # 获取附近的用户
    app.models.User.findAll().success((users)->
      return write_api( res, true, "附近的人", users)
    ).error((errors)->
      return write_api( res, false, "查找附近人错误", null)
    )
  )