module.exports = (app)->
  # MD5 加密函数
  global.md5 = ( text )->
    return require('crypto').createHash('md5').update(text).digest('hex')


  # 判断登陆
  # app.loginUUID 可获取登录者ID
  helper.login_required = (req, res, next)->
    authorization = req.headers.authorization;
    if !authorization?
      return write_api(res, false, "没有传递用户验证信息", { auth:false })
      
    parts = authorization.split(' ')
    auth_type = parts[0]
    auth_string = parts[1]   # 加密过的用户名和密码
    auth_string = require('base64').decode( auth_string ) # 解密
    
    # 解密字符串
    [uuid,password] = auth_string.split(':')
    # 开始数据库验证
    app.models.User.find({
      where: { uuid:uuid }
    }).success((user)->
      if user?
        # 找到
        if user.password == md5( password )
          # 密码正确 ,, 设置UUID
          app.loginUUID = uuid
          next()
        else
          return write_api(res, false, "UUID用户密码错误", { auth:false })
      else
        app.models.User.find({
          where:{ username:uuid }
        }).success((user)->
          # 找到
          if user?
            if user.password == md5( password )
              # 密码正确 ,, 设置UUID
              app.loginUUID = uuid     
              next()
            else
              return write_api(res, false, "Username用户密码错误", { auth:false })
          else
            return write_api(res, false, "UUID,Username都找不到这个用户！:: #{uuid}", { auth:false })
        )
    )
    
  # 公用更新用户位置方法
  helper.updateUserPosition = (latitude, longitude, response, callback)->
    # 开始设置当前用户!
    # 开始设置当前用户!
    app.models.User.find({
      where: {
        uuid : app.loginUUID
      }
    }).success((user)->
      # 更新
      user.updateAttributes({
        recentLatitude : latitude,
        recentLongitude : longitude
      }).success(()->
        console.log "User Position updated"
        if response?  # typeof object
          if callback?
            callback( response ) #callback
          else
            return write_api( response, true, "更新用户位置信息！" )
          
      ).error((errors)->
        console.log "User Position updating Error!"
        if response?
          return write_api( response, false, "更新用户位置信息时数据库出错", errors )
        else
          console.log "No Response found"
      )
    )

  ###
  设置允许跨域Ajax,
  ###
  app.all '/*', (req, res, next)->
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Authorization");
    next()


  # Home
  app.get '/', (req,res)->
    res.send "API Server is running!"

  # 数据库结构同步
  app.get '/admin/syncdb', (req, res)->
    app.models.User.sync()
    res.send "DB Sync!!"



  app.post('/near', helper.login_required, require('./near') )
  app.post('/position', helper.login_required, require('./position'))
  
  app.get('/user/:id?.:format?', helper.login_required, require('./user'))
  app.post('/login', require('./login') )
  app.post('/setting', helper.login_required,require('./setting'))
