module.exports = (req,res)->

    # 判断是否已登陆 (现在使用Basic Auth, 不判断登录了)
    #if req.session.user_uuid?
    #  return write_api(res, false, "登陆过了...", null)
      
      
    uuid = req.body.uuid
    username = req.body.username
    password = req.body.password

    # 首先判断是否用username登陆
    if username? and password?
      # 使用username登陆
      user = app.models.User.find({
        where: {username:username}
      }).error((errors)->
        return write_api(res, false, "查询username用户时出错...", errors )
      ).success (user)->
        # 查询到用户
        if user?
          # 有该用户名
          # 判断密码
          if md5( password ) is user.password
            # 密码正确登陆
            user['connect.sid'] = get_cookie( req, "connect.sid") # session cookie ID 设置
            req.session.user_uuid = uuid
            return write_api(res, true, "username用户登陆成功", user)
          else
            return write_api(res, false, "username用户密码错误！", null)
        else
          return write_api(res,false,"无此username的用户...", null)
    else if uuid?
      # 使用uuid登陆!  如果没有，强制创建，并返回key密码。
      user = app.models.User.find({
        where: { uuid:uuid }
      }).error((errors)->
        return write_api(res, false, "查询UUID用户出错..", errors)
      ).success (user)->
        if !user?
          # 无此用户，创建并登陆吧
          newUserPassword = Math.random().toString();  # 随机密码
          newUser = app.models.User.create({
            uuid:uuid,
            password: md5( newUserPassword ) #MD5加密
          }).success((user)->
            # 创建成功，登陆
            req.session.user_uuid = uuid
            # 输出纯文本密码返回客户端
            user = user.values
            user.newPassword = newUserPassword
            user.auth = true# 提示auth验证成功
            return write_api( res, true, "创建了新的UUID型用户并登陆", user )
            
          ).error((errors)->
            return write_api( res, false, "创建UUID型用户时出错", errors)
          )
        else
          # 有，检查密码 , 登陆
          if !password?
            return write_api( res, false, "此UUID用户登陆需要密码!",null)
          if md5( password ) is user.password
            # 密码正确，登陆
            req.session.user_uuid = uuid
            user = user.values
            user.auth = true # 提醒客户端进行auth
            return write_api( res, true, "UUID型用户登陆", user )
          else
            # 密码错误...
            return write_api(res, false, "通过UUID登陆时密码错误..", null)
    else
      # 缺参数
      return write_api(res, false, "缺少uuid或username参数！", null)

