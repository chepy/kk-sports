# 系统设置
CONFIG = require('./config')

express = require('express') # Web Framework
MySQLSessionStore = require('connect-mysql-session')(express)  # Session plugins



# Create App!
app = module.exports = express.createServer();

app.CONFIG = CONFIG
app.auth = express.basicAuth

# app 配置
app.configure ->
  app.use( express.bodyParser() )  # for use json POST...so on...
  app.use(express.methodOverride());
  #app.use(app.router);

  # Session会话配置，使用MySQL
  app.use(express.cookieParser())
  app.use(express.session {
  store:new MySQLSessionStore(
    app.CONFIG.DB_NAME,
    app.CONFIG.DB_USERNAME,
    app.CONFIG.DB_PASSWORD,
    {
	    defaultExpiration :  1000*60*60*24 * 10 # 3天
    }
  )
  secret:"mrkellysession" # session密匙
  })



module.exports = app