<?php
/**
 * @author Mrkelly chepy.v@gmail.com
 */

	require('PHPExcel.php');
	
	define( 'TITLE_ROW', 2 ); // 定义标题行
	

	/**
	 * 获取xls文件中的api们
	 */
	class ApiXlsxParser {
		/**
		 * 构造器
		 */
		function __construct( $xlsxFile ) {
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objReader->setReadDataOnly(true);
			
			$this->objPHPExcel = $objReader->load( $xlsxFile );
			$this->sheetCount = $this->objPHPExcel->getSheetCount();
		}
		
		/**
		 * 获取所有的表单名词数组
		 */
		function getSheetNames() {
			
			$sheetCount =&  $this->sheetCount; // 一共有多少个表

			$sheetNames = array();
			
			// 循环表
			foreach ( range(0, $sheetCount-1 ) as $sheetIndex ) { // 循环表
				$this->objPHPExcel->setActiveSheetIndex( $sheetIndex );
			
				$objWorksheet = $this->objPHPExcel->getActiveSheet(); // 获取操作表
				
				$sheetNames[] = $objWorksheet->getTitle(); // 加到返回数组里
			}
			
			return $sheetNames;
		}

		
		/**
		 * 获取整个数据表的APIs
		 */
		public function getApis() {
			// $objReader = PHPExcel_IOFactory::createReader('Excel2007');
			// $objReader->setReadDataOnly(true);
			// $objPHPExcel = $objReader->load( $xlsxFile );
			$objPHPExcel =& $this->objPHPExcel;
			$sheetCount =&  $this->sheetCount; // 一共有多少个表
			
			// 开始读表
			$fileResult = array();
			foreach ( range(0, $sheetCount-1 ) as $sheetIndex ) { // 循环表
			
				$objPHPExcel->setActiveSheetIndex( $sheetIndex );
				$objWorksheet = $objPHPExcel->getActiveSheet(); // 获取操作表
				
				$highestRow = $objWorksheet->getHighestRow(); // e.g. 10
				$highestColumnC = $objWorksheet->getHighestColumn(); // e.g 'F' 字符
				$highestCol = PHPExcel_Cell::columnIndexFromString($highestColumnC); // e.g. 8 
				
				
				// 开始读行
				$colConfig = array(); // 设置该表中的列对应的属性项
				
				$sheetResult = array(
					'sheetData' => array()
				);
				foreach( range( TITLE_ROW, $highestRow ) as $row ) { // 循环行
					$rowResult = array(); // 行数据收集
					foreach( range(0, $highestCol-1 ) as $col ) { // 循环列
						$cell = $objWorksheet->getCellByColumnAndRow( $col, $row )->getValue();
						
						// 如果当前在标题行， 预读该列属于什么属性
						if ( $row == TITLE_ROW ) {
							if ( empty($cell) || is_numeric($cell) ) {
								// 无效表单时， 恐怕该表设计有问题，跳出该表循环
								break 3;
							}
							// 有效表单值 , 非空非数字
							$colConfig[ $col ] = $cell;
							continue; // 该行是属性栏， 不用向下读数据处理
						}
						
						// 加入该行一列数据
						$rowResult += array(
							$colConfig[$col] => $cell
						);
					}
					
					// 该行加入
					if ( !empty( $rowResult )) { // 确保不是空行空数据
						array_push( $sheetResult['sheetData'], $rowResult ); // 将一行加入到表单数组
					}
					// 设置sheet信息
					$sheetResult['sheetName'] = $objWorksheet->getTitle();
		
					
					
				}
				
				
				array_push( $fileResult, $sheetResult );
				
				
					
					
			}
	
			return $fileResult;
		}
	}
	
	

 