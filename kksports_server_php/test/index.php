<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>API TEST</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<link rel="stylesheet" href="public/css/bootstrap.min.css"/>
		<!--<script data-main="public/js/main" src="public/js/lib/base64.js"></script>-->
		<script data-main="public/js/main" src="public/js/lib/md5.js"></script>
		<script data-main="public/js/main" src="public/js/require.js"></script>
		<!-- <script src="public/js/require.js"></script>
		<script src="public/js/main-built.js"></script> -->
		
		<link rel="stylesheet" href="public/css/style.css"/>

		<style>
			.tab-content {
				margin-left: 110px;
			}
			.tabs-left .nav-tabs {
				margin-right: 0;
			}
		</style>
	</head>
	<?php
		require('ApiXlsxParser.class.php');
		$parser = new ApiXlsxParser( 'api_doc.xlsx' );
	?>
<body>
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="./">API调试平台</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="">
                <a href="./">首页</a>
              </li>
            </ul>
            
            <p class="navbar-text pull-right">
            	JSON漂亮输出: <span id="nowJsonOutput"></span>
            </p>
            
          </div>
        </div>
      </div>
    </div>
	
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span4">
				<div class="well tabbable tabs-left">
					<div class="alert alert-info">
						<i class="icon-home"></i>
						<b>DeviceID:</b>
						<span id="nowDeviceId">0</span> / 
						
						<span>
							<a id="logout" class="api-btn" href="#" data-api="/user/logout">
								<i class="icon-user"></i>
								<b>UserID:</b>
								<span class="user-id">0</span> / 
								登出
							</a> / 
			        	</span>

			        	<a data-toggle="modal" href="#settingModal" class="">
			        		<i class="icon-edit"></i>
			        		设置
			        	</a>
					</div>
					<table class="table" width="100%">
						<tr>
							<td>
								<b>用户ID:</b>
								<span class="user-id">0</span> , 
								<b>设备ID:</b>
								<span class="user-deviceId">0</span>
							</td>
							<td>
								<b>等级: </b>
								<span class="user-level">0</span> / 
								<b>攻:</b>
								<span class="user-attack">0</span>
								<b>防:</b>
								<span class="user-defense">0</span>
							</td>
						</tr>
						<tr>
							<td>
								<b>生命Health:</b>
								<span class="user-healthCurrent">0</span> / 
								<span class="user-healthMax">0</span>
								(<span class="user-healthCountdown">0</span>)
								<span class="user-healthStep hide">0</span>
							</td>
							<td>
								<b>体力Energy:</b>
								<span class="user-energyCurrent">0</span> / 
								<span class="user-energyMax">0</span>
								(<span class="user-energyCountdown">0</span>)
								<span class="user-energyStep hide">0</span>
							</td>
						</tr>
						<tr>
							<td>
								<b>耐力Stamina:</b>
								<span class="user-staminaCurrent">0</span> / 
								<span class="user-staminaMax">0</span>
								(<span class="user-staminaCountdown">0</span>)
								<span class="user-staminaStep hide">0</span>
							</td>
							<td>
								<b>忠诚Loyalty:</b>
								<span class="user-loyaltyCurrent">0</span> / 
								<span class="user-loyaltyMax">0</span>
								(<span class="user-loyaltyCountdown">0</span>)
								<span class="user-loyaltyStep hide">0</span>
							</td>
						</tr>
						<tr>
							<td>
								<b>现金:</b>
								<span class="user-moneyCurrent">0</span> / 
								<b>存款:</b> 
								<span class="user-bankMoney">0</span>
							</td>
							<td>
								<b>经验:</b>
								<span class="user-xp">0</span> / 
								<span class="user-nextLevelXp">0</span>
							</td>
						</tr>
					</table>
					<ul class="nav nav-tabs tabs">
					<?php
						$apis = $parser->getApis();
						
						foreach( $apis as $key=>$apiSheet ):
					?>
					<?php //echo $key == 0 ? 'active' : '' ?>
						<li>
							<a href="#tab-<?php echo $key;?>" data-toggle="tab">
								<?php echo $apiSheet['sheetName']; ?>
							</a>
						</li>
					<?php
						endforeach;
					?>
					</ul>

					
					<div class="tab-content">
						<?php
							
							foreach( $apis as $key=>$apiSheet ):
						?>
						 <?php // echo $key == 0 ? 'active' : '' ?>
						<ul id="tab-<?php echo $key;?>" class="tab-pane nav nav-list">
							<li class="nav-header">
								<?php echo $apiSheet['sheetName']; // 表名 ?>
							</li>
								<?php
									foreach( $apiSheet['sheetData'] as $key=>$apiRow ):
										//API一条
										if ( $apiRow['title'] ): // 防止空行
								?>
								<li>
									<a href="#" class="api-btn"
										data-api="<?php echo $apiRow['api'];?>"
										data-method="<?php echo $apiRow['method'];?>"
										data-model="<?php echo $apiRow['model'];?>"
										rel="tooltip"
										data-original-title="<?php echo $apiRow['method'];?> : <?php echo $apiRow['api'];?>">
										
										<?php if ( !empty( $apiRow['ok']) ) : ?>
										<i class="icon-book"></i>
										<?php endif; ?>
										<?php echo $apiRow['title']; ?>
										
									</a>
									<textarea class="data-api-test-arg hide"><?php echo $apiRow['test-arg'];?></textarea>
									<textarea class="data-api-test-obj hide"><?php echo $apiRow['test-obj'];?></textarea>
									<textarea class="data-api-content hide"><?php echo htmlspecialchars($apiRow['content']);?></textarea>
									<textarea class="data-api-demo hide"><?php echo htmlspecialchars($apiRow['demo']);?></textarea>
									
								</li>
								<?php
										endif;
									endforeach;
								?>
						</ul>
						<?php
							endforeach;
						?>
					</div>
				</div>
				
				
				<form action="/kksports/index.php/upload_avatar" method="POST" enctype="multipart/form-data">
					<input type="file" name="userfile" />
					<!--<button type="submit">上传文件测试</button>-->
					<input type="submit" value="上传！" />
				</form>
				
			</div>
			
			<div class="span8">
				<div class="page-header">
					<h1 class="api-title">
						<small class="api-url">API-URL</small>
					</h1>
				</div>
				<div class="alert alert-error hide">
					API调用错误！ （可能未完善）
				</div>
				<div class="alert alert-success hide">
					API测试调用成功！
				</div>
				
				<table class="table table-bordered table-striped table-condensed">
					<tbody>
						<tr>
							<th colspan="2">
								<h3>资料</h3>
							</th>
						</tr>
						
						<tr>
							<td width="20%">方法: </td>
							<td>
								<span class="api-method"></span>
							</td>
						</tr>
						<tr>
							<td>返回数据模型</td>
							<td>
								<span class="api-model"></span>
							</td>
						</tr>

						<tr>
							<td>介绍</td>
							<td><div class="api-content"></div></td>
						</tr>
						<tr>
							<td>可能返回数据</td>
							<td><div class="api-demo"></div></td>
						</tr>
						
						<tr>
							<th colspan="2">
								<h3>测试 (提供模拟数据进行测试)</h3>
							</th>
						</tr>
						<tr>
							<td>测试参数(Arg)</td>
							<td>
								<div class="api-test-arg"></div>
								<!-- <span class="help-block">
									用于替换网址中的参数
								</span> -->
							</td>
						</tr>

						<tr>
							<td>测试数据(POST OBJ)</td>
							<td>
								<input type="text" class="api-test-obj" />
								<span class="help-block">
									<i class="icon-pencil"></i>
									修改可用于自定义执行
								</span>
							</td>
						</tr>
						<tr>
							<td>测试API URL</td>
							<td>
								<input type="text" class="api-test-url" />
								<span class="help-block">
									<i class="icon-pencil"></i>
									修改可用于自定义执行
								</span>
							</td>
						</tr>
						<tr>
							<td>执行返回JSON数据:</td>
							<td>
								<textarea style="width:90%;height: 250px;" class="api-json"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="pull-right">
		            <a class="custom-api-btn btn btn-primary">自定义执行</a>
	          	</div>
			</div>
		</div>
	</div>
	
	
	<!-- Setting Modal -->
	<div class="modal hide fade" id="settingModal">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>API测试系统设置</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="deviceId">设备ID(Device ID)</label>
					<div class="controls">
						<input type="text" name="deviceId" id="settingDeviceId" />
						<p class="help-block">
							设备ID用于登录
						</p>
					</div>
				</div>
				<div class="control-group">
					<label for"jsonOutput">漂亮JSON输出</label>
					<div class="controls">
						<input type="checkbox" name="jsonOutput" id="settingJsonOutput">
						<p class="help-block">
							采用漂亮json输出强制json格式，对于程序出错，可能无法看到
						</p>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" id="settingSubmit" class="btn btn-primary">保存设置</a>
			<a data-dismiss="modal" href="#" class="btn">关闭</a>
		</div>
	</div>
</body>
</html>