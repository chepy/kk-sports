/**
 * Local Storage Module
 */
var KK = {};

define( [ 'lib/jquery', 'lib/json2', 'bootstrap' ], function() {
	
	(function(){
		
		var _KK = arguments[0];
		
		// 本地储存
		if ( !window.localStorage ) {
			alert( '你的浏览器好旧啊，换成支持HTML5的浏览器（如Chrome，Safari），不然有些功能用不到啊');
		}
		
		_KK.setOption = function( key, value ) {
			window.localStorage[key] = value;
		}
		
		_KK.getOption = function( key ) {
			return window.localStorage[key];
		}
		
		/**
		 * 数学包
		 */
		_KK.Math = {
			randInt : function(min, max){
				return Math.floor( (Math.random() * ( max- min +1 )) + min);
			}
		}
		
		/**
		 * JSON包 内含
		 */
		 // JSON对象再用
		_KK.JSON = {
			stringify : JSON.stringify,
			parse : function( str ) {
				try {
					json = JSON.parse( str );
					return json;
				} catch( e ) {
					return false;
				}
			}
		};
		
	})(KK);
	
	return KK;
});

