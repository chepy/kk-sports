define( [ 'KK', 'page_fn' ], function( KK ) {
	// 倒计时
	setInterval( 'KK.page.countdown()', 1000 );
	
	$(function(){
		// 页面初始化
		// 页面变动事件绑定
		$('.modal').bind('show', KK.page.page_init );
		KK.page.user_init( KK.JSON.parse( KK.getOption('userProfile') ) );// 读取用户
		KK.page.page_init(); // 初始化
		

		
		/**
		 * 激活哪个标签
		 */
		function active_tab() {
			activeTab = KK.setOption('activeTab', $(this).attr('href') );
		}
		$('.tabs > li > a').bind('click', active_tab); // 点击标签，储存标签
		
		
		/**
		 * 设置选项
		 */
		setting_form = function(){
			// 设置 deviceId
			newDeviceId = $('#settingDeviceId').val();
			// 判断是否改变了deviceId,是的话登出
			if ( newDeviceId != KK.getOption('deviceId') ) {
				KK.setOption( 'deviceId', newDeviceId );
				$('#logout').trigger('click');// 强制登出	
			}
			
			// JSON输出形式
			jsonOutput = $('#settingJsonOutput').attr('checked');
			if ( !jsonOutput ) jsonOutput = ''; // 翻译undefined 
			KK.setOption( 'jsonOutput',  jsonOutput);
			
			KK.page.page_init(); // 页面改变
			$('.modal').modal('hide'); // 关闭窗口
		}
		
		$('.modal').bind('keypress', function(e){ // 回车确定
			if ( e.which == 13 ) {
				setting_form();
				e.preventDefault();
			}
		});
		
		$('#settingSubmit').bind('click', setting_form); // 鼠标确定
		
		
		
		
		// API 按钮
		$('.api-btn').click(function(){
			_method = $(this).data('method');
			_api_url = $(this).data('api');
			
			_arg = $(this).parent().find('.data-api-test-arg').val(); // 网址参数
			if ( !_arg ) _arg = '{}';
			_arg_json = KK.JSON.parse( _arg ); // txt2json 
			
			_object = $(this).parent().find('.data-api-test-obj').val(); // POST Obj
			if ( !_object ) _object = '{}'; // 空传入对象
			_object_json = KK.JSON.parse ( _object );
			
			
			// 完整的、带有参数覆盖的API URL
			var total_api_url = _api_url; 
			
			// 用参数替换URL
			for ( var key in  _arg_json ) {
				total_api_url = total_api_url.replace( '{' + key + '}' ,  _arg_json[key] );
			}
			
			// Ajax start
			var _method = _method;
			var _url = total_api_url;
			var _data = _object_json;
			
			api_exe( _method, _url, _data );// 执行api

			
			// API信息
			$('.api-title').html( $(this).text() + '<small class="api-url">' + _api_url  + '</small>' );
			$('.api-method').text( _method );
			$('.api-model').text( $(this).data('model') );
			
			$('.api-test-url').val( total_api_url );
			$('.api-test-arg').text( $(this).parent().find('.data-api-test-arg').val() );
			$('.api-test-obj').val( $(this).parent().find('.data-api-test-obj').val() );
			
			$('.api-content').text( $(this).parent().find('.data-api-content').val() );
			$('.api-demo').text( $(this).parent().find('.data-api-demo').val() );
			
			// 页面效果
			$(this).parent().parent().find('li.active').removeClass('active'); //取消其它激活
			$(this).parent().addClass('active');
			// 修改href, 便于调试
			$(this).attr('href', api_host + total_api_url );
			return false;
		});
		
		/**
		 * 自定义执行 按钮
		 */
		$('.custom-api-btn').click(function(){
			var	_method = $('.api-method').text();
			var _url = $('.api-test-url').val();
			var _data = KK.JSON.parse( $('.api-test-obj').val() );
			
			api_exe( _method, _url, _data ); // 执行api
		});
		
		
		function api_exe( _method, _url, _data ) {
			// Ajax start
			$.ajax({
				type: _method,  // GET or POST
				url: api_host + _url,
				data: _data,
				crossDomain: true,
				xhrFields: {
						//withCredentials: false
						//'Set-Cookie':true
				},
				beforeSend: function( xhr, settings ) {
				
						// function make_base_auth(user, password) {
							// var tok = user + ':' + password;
							// var hash = Base64.encode(tok);
							// return "Basic " + hash;
						// }
						// auth = make_base_auth( KK.getOption('auth_uuid'), KK.getOption('auth_password') );
						
						// 如果有保存了登录信息
						if ( KK.getOption('auth_uuid') != null )
						{
							auth = KK.getOption('auth_uuid') + ':' + hex_md5( KK.getOption('auth_password'));
							xhr.setRequestHeader('Authorization', auth);
						}
					//xhr.setRequestHeader('Cookie', document.cookie);
					//xhr.setRequestHeader('Access-Control-Allow-Credentials', 'true');
					//xhr.setRequestHeader('d', KK.getOption('deviceId') ); // DeviceID
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('.alert-error').show();
					$('.alert-success').hide();
					$('.api-json').html( "Fail: " + textStatus );
				},
				success: function(data, status, xhr) {
					
					// 是否漂亮输出?
					if ( KK.getOption('jsonOutput') == 'checked' ) {
						// 是
						try {
							json = KK.JSON.parse( data );
							result = KK.JSON.stringify( json, null, '\t');
						} catch( e ) {
							
							result = 'json parse error please change output: ' + e;
						}
					} else {
						//json = KK.JSON.parse( data );
						try
						{
							json = $.parseJSON(data);
						}
						catch(e)
						{
							json = {};
						}
						result = data;
					}
					
					
					if ( json.data )
					{
						if ( json.data.auth == '1' )
						{
							KK.setOption('auth_uuid', json.data.uuid);
							KK.setOption('auth_password', json.data.newPassword);
						}
					}

					// 如果返回了json格式的对象， 设置页面用户信息显示
					if ( json.user ) {
						
						KK.page.user_init( json.user );
						
						KK.setOption( 'userProfile', JSON.stringify( json.user ) ); // 保存信息到本地, 便于刷新读取
					}
					
					
					$('.api-json').html( result );
					
					$('.alert-error').hide();
					$('.alert-success').show();
	
				},
				dataType: 'text'
			});
		}
	});
	
	
});
