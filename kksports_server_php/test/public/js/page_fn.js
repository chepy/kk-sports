// 关于页面显示的一切
// 页面处理函数
define( [ 'KK' ], function( KK ) {

		/**
		 * 页面数据初始化
		 */ 
		function page_init() {
			
			// jquery 动态
			$('*[rel=tooltip]').tooltip();
			
			var deviceId = KK.getOption('deviceId');
			
			if ( !deviceId ) {
				// deviceId没有储存在local storage?  随机生成一个
				newDeviceId = KK.Math.randInt( 1, 1000 );
				deviceId = newDeviceId;
				KK.setOption('deviceId', newDeviceId );
			}
			
			$('#nowDeviceId').text( deviceId );
			// 设置窗口			
			$('#settingDeviceId').val( deviceId );		
			
			
			// 是否漂亮JSON输出
			var jsonOutput = KK.getOption('jsonOutput');
			if (jsonOutput) {
				$('#settingJsonOutput').attr('checked', 'checked' );
				$('#nowJsonOutput').text('是');
			} else {
				$('#nowJsonOutput').text('否');
			}
			
			// 激活之前使用过的标签
			activeTab = KK.getOption('activeTab');
			
			//alert( activeTab );
			if ( !activeTab ) activeTab = '#tab-0';
			$('.tabs a[href=' + activeTab + ']').tab('show');
			
		}
		


		
		/**
		 * 初始化页面的用户信息数据
		 * 
		 * @param user_obj json
		 */
		function user_init( user_obj ) {
			// 循环显示所有user object 属性
			for ( var key in user_obj ) {
				$('.user-' + key ).text( user_obj[key] )
			}
		}
		

		
		/**
		 * 页面倒数秒数
		 */
		function countdown( ) {
			
			var attrs = ['health', 'energy', 'stamina', 'loyalty'];
			for ( var key in attrs ) {
				countdown_element = $('.user-' + attrs[key] + 'Countdown'); 
				_countdown =  parseInt( countdown_element.text() );
				
				current_element = $('.user-' + attrs[key] + 'Current');
				max_element = $('.user-' + attrs[key] + 'Max');
				
				current_attr = parseInt( current_element.text() );
				max_attr = parseInt( max_element.text() );
				
				// 是否已经最大值了
				if ( current_attr < max_attr) {
					_countdown -= 1;  // 倒数  -1
					if ( _countdown == 0 ) {
						// 倒数到0了啊！
						// 行动
						current_element.text( current_attr + 1 );
						
						// 如果增加属性知道还没到max， 继续倒数, 设置间隔时间
						if ( current_attr + 1 < max_attr ) {
							_countdown = $('.user-' + attrs[key] + 'Step').text();
						}
					}
					countdown_element.text( _countdown ); // 倒计时				
				} // else 已经最大值了，不做事
				
				
			}
		}
		
		KK.page = {};
		KK.page.page_init = page_init;
		KK.page.user_init = user_init;
		KK.page.countdown = countdown;
		
		return KK;
});


		

