<?php

	class KK_Model extends CI_Model
	{

		function insert($object)
		{
			return $this->db->insert($this->db_name, $object);
		}
		
		function update($data, $where_array)
		{
			return $this->db->update($this->db_name, $data, $where_array);
		}
		
		// 获取表的所有
		function getAll()
		{
		    return $this->db->get($this->db_name);
		}
	}