<?php
    
    class KK_Controller extends CI_Controller
    {
		var $loginUUID = null;   // 登录账户记录
		
		var $noLoginCheck = array(   // 这些网址无需进行登录验证
			'login',
			'upload_avatar',
		);
		
        function __construct()
		{
			parent::__construct();
			
			$current_uri = $this->uri->uri_string;  // 当前进入的网址
			
			foreach($this->noLoginCheck as $u)
			{
				if ($current_uri == $u)
				{
					return;
				}
			}

			
			$auth = $this->input->get_request_header('Authorization');
			if (!empty($auth))
			{
				list($auth_uuid, $auth_md5pwd) = explode(':', $auth);
				
				// 开始验证
				$user = $this->User_model->get_user_by_uuid($auth_uuid);
				if (empty($user))
				{
					api_write(false, '无法找到该UUID的用户', array('uuid' => $auth_uuid,));
				}
				else
				{
				    $user = $user->row();
				    
					// 密码判断
					if ($user->password == $auth_md5pwd)
					{
						$this->loginUUID = $auth_uuid;
						return; //通过登录验证
					}
					else
					{
						api_write(false, '登录密码错误！', array(
							'uuid' => $auth_uuid, 
							'password' => $auth_md5pwd,
						));
					}
				}
			}
			else
			{
				api_write(false, '没有用户登录信息 - Authorization', null);
			}
			
		}
    }