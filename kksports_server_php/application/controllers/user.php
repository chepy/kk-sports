<?php
	class User extends KK_Controller
	{
		function __construct()
		{
			parent::__construct();

			$this->load->model('User_model');
        	$this->output->set_header('Content-type:text/html; charset=utf-8');
		}
		
		function user_self()
		{
			// 查看用户自己
			$user = $this->User_model->get_user_by_uuid($this->loginUUID);
			api_write(true, '用户自己', $user->row_array());
		}
		
		function user_lookup($uuid)
		{
		    $user = $this->User_model->get_user_by_uuid($uuid);
		    if (!empty($user))
		    {
				$user_array = $user->row_array();
				unset($user_array['password']); // 除开密码显示
		        api_write(true, "返回用户", $user_array);
		    }
		    else
		    {
		        api_write(false, "找不到用户", null);
		        
		    }
		}
		
		
		function setting()
		{
			$to_set = array(
				'nickname' => $this->input->post('nickname'),
				'username' => $this->input->post('username'),
			);
			
			// 去除空的设置属性
			$to_set = array_filter($to_set);
			
			if ( !empty($to_set))
			{
				$this->User_model->update(
					$to_set,
					array(
						'uuid'=>$this->loginUUID,
					)
				);
				api_write(true, 'setting user', $to_set);
			}
			else
			{
				api_write(false, '没有传入要修改的属性', null);
			}
			
			
		}
		
		// 上传头像
		function upload_avatar()
		{
			if (class_exists('SaeStorage'))
			{
				echo '!!! yes';
			}
			// $upload_cfg['upload_path'] = './uploads/';
			// $upload_cfg['allowed_types'] = 'gif|jpg|png';
			// $upload_cfg['max_size'] = '1024';
			// $upload_cfg['max_width'] = '1024';
			// $upload_cfg['max_height'] = '768';
			
			// $this->load->library('upload', $upload_cfg);
			
			// if (!$this->upload->do_upload('userfile'))
			// {
				// api_write(false, $this->upload->display_errors(), null);
			// }
			// else
			// {
				// // 上传成功
				// api_write(true, '上传成功！', $this->upload->data());
			// }
		}
		
		function login()
		{
		    $uuid = $this->input->post('uuid');
		    $username = $this->input->post('username');
		    $password = $this->input->post('password');
			
		    if (!empty($username) && !empty($password))
		    {
		        // 使用账号密码登录
		        $user = $this->User_model->get_user_by_username($username);
		        if (empty($user))
		        {
		            api_write(false, "找不到这个用户名的用户", null);
		        }
		        else
		        {
		        
		            api_write(true, "找到用户，todo", $user->result_array());
		        }
		    }
		    else if (!empty($uuid))
		    {
		        // 使用UUID登录
		        $user = $this->User_model->get_user_by_uuid($uuid);
		        
		        if (empty($user))
		        {
		            // 开始注册..
		            $newPassword = rand();  // 随机密码
		            $newUser = new UserObject();
		            $newUser->uuid = $uuid;
		            $newUser->password = md5($newPassword);
		            
		            $this->User_model->insert($newUser);
		            $return = array(
		                'newPassword' => $newPassword,
		                'uuid' => $uuid,
		                'auth' => true,
		            );
		            api_write(true, "第一次登录自动注册", $return);
		        }
		        else
		        {
		            api_write(true, "UUID找到用户登录, todo", $user->row_array());
		        }
		    }
		    else
		    {
		        api_write(false, "缺少uuid或username参数！", null);
		    }
		    
		}
		
	}