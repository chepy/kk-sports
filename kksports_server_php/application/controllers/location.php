<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Location extends KK_Controller
	{
	    function __construct()
	    {
	        parent::__construct();
	        $this->load->model('User_model');
	    }
	    
	    function near_post()
	    {
            $this->response(array('fuck'=>'f', 'ok'=>'okkk',),200);
	    }
	    // 当前用户查看附近的人
	    public function near()
	    {
			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');
			if ($latitude == null && $longitude == null)
			{
				api_write(false, "需要传入最新的坐标参数！", null);
				return;
			}
			
			$this->RePosition($latitude,$longitude);
			
	        $users = $this->User_model->getAll();
			$users_array = $users->result_array();
			
			// 去掉密码列
			foreach($users_array as $key=>$value)
			{
				unset($users_array[$key]['password']);
			}
	        api_write(
	            true, 
	            sprintf("%f, %f, 附近的人", $latitude, $longitude), 
	            $users_array
	        );
	    }
	    
		public function position()
		{
			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');
			
			if ($latitude != null && $longitude != null)
			{
				$this->RePosition($latitude, $longitude);
				api_write(true, "重新定位成功", array(
					'newLatitude' => $latitude,
					'newLongitude' => $longitude,
				));
			}
			else
			{
				api_write(false, "缺少经纬度参数吧？", array(
					'postLatitude' => $latitude,
					'postLongitude' => $longitude,
				));
			}
		}
		
	    // 重新定位用户位置
	    private function RePosition($latitude, $longitude)
	    {
			$new_pos = array(
				'recentLatitude' => $latitude,
				'recentLongitude' => $longitude,
			);
			
			// Save
			$this->User_model->update(
				$new_pos,
				array(
					'uuid' => $this->loginUUID,
				)
			);
			
			
			
	    }
	}